#include <stdio.h>
#include <signal.h>

void sigint_handler(int signo)
{
    printf("Señal encontrada!!!\n");
}

int main()
{
    signal(SIGINT, sigint_handler);
    while (1);
}

// ctrl-z
// ps
// kill -9 25947