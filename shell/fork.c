#include <stdio.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    pid_t child_pid = fork();

    // Proceso hijo
    if (child_pid == 0)
    {
        printf("Hijo\n PID: %d y PID Hijo: %d\n", getpid(), child_pid);
    }
    else
    {
        sleep(1); // Duerme por un segundo
        printf("Padre\n PID: %d y PID Hijo: %d\n", getpid(), child_pid);
    }

    return 0;
}
