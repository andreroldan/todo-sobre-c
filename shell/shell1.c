#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <readline/readline.h>

char **get_input(char *input);

int main(int argc, char const *argv[])
{
    char **comandos;
    char *input;
    pid_t child_pid;
    int stat_loc;

    while (1)
    {
        input = readline("nuestrashell$ ");
        comandos = get_input(input);

        if (!comandos[0])
        {
            free(input);
            free(comandos);
            continue;
        }

        child_pid = fork();

        if (child_pid < 0)
        {
            perror("Fork falló");
            exit(1);
        }

        if (child_pid == 0)
        {
            // Nunca regresa si la llamada fue exitosa
            if (execvp(comandos[0], comandos) < 0)
            {
                perror(comandos[0]);
                exit(1);
            }
            
        }
        else
        {
            waitpid(child_pid, &stat_loc, WUNTRACED);
        }
        free(input);
        free(comandos);
    }
    
    return 0;
}

char **get_input(char *input)
{
    char **comando = malloc(8 * sizeof(char *));
    if (comando == NULL)
    {
        perror("malloc falló");
        exit(1);
    }
    
    char *separador = " ";
    char *parsed;
    int index = 0;

    parsed = strtok(input, separador);
    while (parsed != NULL)
    {
        comando[index] = parsed;
        index++;

        parsed = strtok(NULL, separador);
    }

    comando[index] = NULL;
    return comando;   
}

// gcc shell1.c -o shell1 -Wall -lreadline