#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
    pid_t child_pid;
    pid_t wait;
    int stat_loc;

    child_pid = fork();
    // Proceso Hijo
    if (child_pid == 0)
    {
        printf("Hijo\n PID: %d y PID Hijo: %d\n", getpid(), child_pid);
        sleep(1);
    }
    else
    {
        wait = waitpid(child_pid, &stat_loc, WUNTRACED);
        printf("Padre\n PID: %d y PID Hijo: %d\n", getpid(), child_pid);
    }
    
    return 0;
}
