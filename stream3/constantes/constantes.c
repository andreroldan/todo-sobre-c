#include <stdio.h>

int main(int argc, char const *argv[])
{
    // Variable
    int miEdad = 26;

    printf("Mi edad es: %d\n", miEdad);
    miEdad = miEdad + 1;
    printf("Paso un año, y ahora tengo %d años\n", miEdad);

    //Constantes
    const int NUMERO_FAVORITO = 8;
    printf("Mi número favorito es: %d\n", NUMERO_FAVORITO);

    // Esto da error, no podemos modificar una variable constante
    // NUMERO_FAVORITO = NUMERO_FAVORITO + 1;
    // printf("Los giros que da la vide, ahora mi número fav es: %d\n", NUMERO_FAVORITO);
    return 0;
}
