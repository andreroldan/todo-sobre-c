#include <stdio.h>

int main(int argc, char const *argv[])
{
    // Arreglo
    int serieNumeros[] = {7, 8, 22, 94, 60, 101};
    printf("El primer valor de mi serie de números es: %d\n", serieNumeros[0]);
    printf("El sexto valor de mi serie de números es: %d\n", serieNumeros[5]);
    char saludo[] = "Hola";
    printf("%s chat son los mejores\n", saludo);
    return 0;
}
