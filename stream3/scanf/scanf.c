#include <stdio.h>

int main(int argc, char const *argv[])
{
    // scanf
    int edad;
    printf("Ingresa tu edad: ");
    scanf("%d", &edad);
    printf("La edad del usuario es: %d\n", edad);

    // fgets
    char nombre[40];
    printf("Ingresa tu nombre: ");
    scanf("%s", nombre);
    //fgets(nombre, 40, stdin);
    printf("Tu nombre es: %s\n", nombre);
    return 0;
}