#include <stdio.h>

/* 
Condicionales
== : Es igual
!= : No es igual
<  : Menor que
>  : Mayor que
<= : Menor o igual que
>= : Mayor o igual que

&& : (y)solo si ambas condiciones se cumplen.
|| : (o)si cualquiera de las condiciones se cumplen
*/
int numeroMayor(int num1, int num2, int num3);

int main(int argc, char const *argv[])
{
    printf("El número más grande es: %d\n", numeroMayor(5, 7, 1));
    printf("El número más grande es: %d\n", numeroMayor(144, 27, 54));
    printf("El número más grande es: %d\n", numeroMayor(8, 8, 8));
    printf("El número más grande es: %d\n", numeroMayor(-23, -3, -79));
    return 0;
}

int numeroMayor(int num1, int num2, int num3)
{
    int resultado;
    if (num1 == num2 && num1 == num3)
    {
        printf("Los tres numeros son iguales: ");
        return num1;
    }
    else if (num1 >= num2 && num1 >= num3)
    {
        resultado = num1;
    }
    else if (num2 >= num1 && num2 >= num3)
    {
        resultado = num2;
    }
    else
    {
        resultado = num3;
    }
    return resultado;
}