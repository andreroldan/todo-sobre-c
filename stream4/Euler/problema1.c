/*
Si hacemos una lista de todos los números naturales menores a 10
que son multiplos de 3 o 5 tenemos el 3, 5, 6 y 9. La suma de 
estos multiplos es 23.
Encuentra la suma de todos los multiplos de 3 o 5 menores a 1000.

Autores: enriqueabsurdum, Ln0xx, jaimebedoyae, rookiec137.
*/

#include <stdio.h>

int sumaMultiplos3y5(int numero);

int main(int argc, char const *argv[])
{
    printf("La suma de todos los multiplos de 3 o 5 es: %d\n", sumaMultiplos3y5(1000));
    return 0;
}

int sumaMultiplos3y5(int numero)
{
    int suma = 0;
    for (int i = 1; i < numero; i++)
    {
        if (i % 3 == 0 || i % 5 == 0)
        {
            suma += i;
        }
    }
    return suma;
}
