/*
Cada nuevo termino en la secuencia de Fibonacci es generado por la suma de los dos
terminos previos. Empezando por 1 y 2, los 10 primeros terminos son:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

Encuentra la suma de todos los terminos pares que no excedan los 4 millones.

Autores: guilleramz, Ln0xx, enriqueabsurdum, amarok_w, systmcrack
:
*/

#include <stdio.h>

int sumaParesFibonacci(int numero);

int main(int argc, char const *argv[])
{
    printf("La suma de todos los números en la secuencia de Fibonacci es: %d\n", sumaParesFibonacci(4000000));
    return 0;
}

int sumaParesFibonacci(int numero)
{
    int x = 1;
    int y = 2;
    int suma = y;
    while (y < numero)
    {
        int temp = x;
        x = y;
        y = temp + y;
        if (y % 2 == 0)
        {
            suma += y;
        }
        
    }
    return suma;
}
