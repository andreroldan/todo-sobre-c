#include <stdio.h>

int sumaDosEnteros (int a, int b);

int main()
{
    int x = 2;
    int y = 3;
    int resultado = sumaDosEnteros(x, y);
    printf("El resultado es %d\n", resultado);
    return 0;
}

int sumaDosEnteros (int a, int b) {
    int c = a + b;
    return c;
}
