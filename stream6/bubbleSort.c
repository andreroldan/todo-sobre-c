#include <stdio.h>

void ordenamientoBurbuja(int arr[], int tamaño);
void cambiar(int *pJ1, int *pJ2);
void imprimirArreglo(int arr[], int tamaño);

int main(int argc, char const *argv[])
{
    int arreglo[] = {7, 54, 78, 56, 4, 24, 56, 10, 11, 19, 21, 24, 93, 1001, 900001, 54};
    int tamaño = sizeof(arreglo)/sizeof(arreglo[0]);
    ordenamientoBurbuja(arreglo, tamaño);
    printf("El arreglo ordenado:\n");
    imprimirArreglo(arreglo, tamaño);
    return 0;
}

void ordenamientoBurbuja(int arr[], int tamaño)
{
    for (int i = 0; i < tamaño-1; i++)
        for (int j = 0; j < tamaño-i-1; j++)
            if (arr[j] > arr[j+1])
                cambiar(&arr[j], &arr[j+1]);    
}

void cambiar(int *pJ1, int *pJ2)
{
    int temporal = *pJ1;
    *pJ1 = *pJ2;
    *pJ2 = temporal;
}

void imprimirArreglo(int arr[], int tamaño)
{
    for (int i = 0; i < tamaño; i++)
        printf("%d\n", arr[i]);
}