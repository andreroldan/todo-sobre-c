#include <stdio.h>
#include <math.h>

float inversaRaizCuadrada(float x);

int main(int argc, char const *argv[])
{
    float n = 9;
    float r = inversaRaizCuadrada(n);
    printf("%f\n", r);
    return 0;
}

float inversaRaizCuadrada(float x)
{
    float y = 1 / sqrt(x);
    return y;
}