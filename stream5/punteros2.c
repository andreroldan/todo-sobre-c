#include <stdio.h>

int main(int argc, char const *argv[])
{
    int edad = 26;
    int *pEdad = &edad;
    int **ppEdad = &pEdad;
    // int ***pppEdad = &ppEdad;

    printf("edad = %d\n", edad);
    printf("direccion edad = %p\n", &edad);
    printf("pEdad = %p\n", pEdad);
    printf("direccion pEdad = %p\n", &pEdad);
    printf("ppEdad = %p\n", ppEdad);
    printf("ppEdad = %p\n", &ppEdad);

    printf("pEdad desreferenciado  = %d\n", *pEdad);
    printf("ppEdad desreferenciado = %d\n", **ppEdad);
    


    /*
    float f = 69.420;
    float *pF = &f;

    char c = '!';
    char *pC = &c;

    printf("edad = %d\n", edad); //26
    printf("direccion edad = %p\n", &edad); //0x99
    printf("pEdad = %p\n", pEdad); //0x99
    printf("el valor al que apunta pEdad es = %d\n", *pEdad); // 26
    printf("la direccion de pEdad = %p\n", &pEdad); //0x789

    printf("f = %f\n", f); //69.420
    printf("pF = %p\n", pF); //0x123

    printf("c = %c\n", c); //!
    printf("pC = %p\n", pC); //0x473
    */
    
    return 0;
}
