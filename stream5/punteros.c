#include <stdio.h>

int main(int argc, char const *argv[])
{
    int edad = 26;
    int *pEdad = &edad;

    /*
    printf("El valor de edad es: %d\n", edad);
    printf("La direccion de memoria de edad es: %p\n", &edad);
    printf("pEdad está apuntando a: %p\n", pEdad);
    printf("La direccion de memoria de pEdad es: %p\n", &pEdad);
    printf("El valor al que está apuntando pEdad es %d\n", *pEdad);
    */
   
    printf("La direccion a la que apunta pEdad es: %d\n", pEdad);
    printf("El tamaño de un entero es: %d bytes\n", sizeof(edad));
    printf("La direccion a la que apunta pEdad + 1 es: %d\n", pEdad + 1);
    printf("El valor al que está apuntando pEdad es: %d\n", *pEdad);
    pEdad = pEdad + 1;
    printf("El valor al que está apuntando pEdad + 1 es: %d\n", *pEdad);
    pEdad = pEdad - 1;
    printf("El valor al que está apuntando pEdad es: %d\n", *pEdad);
    return 0;
}
