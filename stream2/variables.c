#include <stdio.h>

int main(int argc, char const *argv[])
{
    char seguidor1[] = "cuervo2008";
    char seguidor2[] = "marcosfunk93";

    printf("Hola saludos en el chat a %s y a %s\n", seguidor1, seguidor2);
    printf("%s es muy bueno en C#, está creando su propio sistema operativo\n", seguidor2);
    printf("%s llegó con el raid de Zkuaker, es un experto en cyberseguridad\n", seguidor1);
    return 0;
}