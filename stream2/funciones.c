#include <stdio.h>

int sumaDosEnteros(int a, int b);
void imprimeHola();

int main(int argc, char const *argv[])
{
    int x = 5;
    int y = 7;
    int resultado = sumaDosEnteros(x, y);
    printf("La suma de x + y = %d\n", resultado);

    int z = 78;
    resultado = sumaDosEnteros(x, z);
    printf("La suma de x + z = %d\n", resultado);

    imprimeHola();
    return 0;
}

int sumaDosEnteros(int a, int b)
{
    int suma = a + b;
    return suma;
}

void imprimeHola()
{
    printf("Hola chat, gracias por seguir aquí <3\n");
}
