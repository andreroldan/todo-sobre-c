#include <stdio.h>

int main(int argc, char const *argv[])
{
    char queSigue;
    printf("¿Qué sigue para el canal? ");
    scanf("%c", &queSigue);

    switch(queSigue) {
        case 'A' :
        printf("¡Aprendamos GO!\n");
        break;

        case 'B' :
        printf("Un stream sobre Linux :D\n");
        break;

        case 'C' :
        printf("Veamos el kernel de linux >:B\n");
        break;

        case 'D' :
        printf("Un curso completo de Python\n");
        break;

        default :
        printf("Pues a seguir ahora con C++\n");
    }
    return 0;
}
