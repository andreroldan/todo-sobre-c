#include <stdio.h>
#include <string.h>

struct Estudiante
{
    char nombre[40];
    char carrera[40];
    int edad;
    float estatura;
    struct ComidaFavorita
    {
        char comida1[20];
        char comida2[20];
    };
};


int main(int argc, char const *argv[])
{
    struct Estudiante estudiante1;
    estudiante1.edad = 28;
    estudiante1.estatura = 1.97;
    struct ComidaFavorita deEstudiante1;
    strcpy( deEstudiante1.comida1, "Chilaquiles");
    strcpy( estudiante1.nombre, "Marcos");
    strcpy( estudiante1.carrera, "SoftwareDeveloper");

    printf("%s\n", estudiante1.carrera);
    printf("%d\n", estudiante1.edad);
    printf("%s\n", deEstudiante1.comida1);

    struct Estudiante estudiante2;
    strcpy( estudiante2.nombre, "André");
    strcpy( estudiante2.carrera, "Sistemas");

    printf("%s\n", estudiante2.nombre);
    printf("%s\n", estudiante2.carrera);

    
    return 0;
}
